{{-- ===== --}}
{{-- HOOKS --}}
{{-- ===== --}}

{{-- By order of appearence --}}
{{-- Do not delete, keep them empty if unused --}}

{{-- init --}}
@task('hook_init')
    echo "=> Starting deployment..."
    {{-- cd {{ $config['app_path'] }} --}}
    {{-- php {{ $config['composer_path'] }}/wp-cli.phar maintenance-mode activate --allow-root --}}
@endtask

{{-- git --}}
@task('hook_git_before')
    echo "==> Start git"
@endtask

@task('hook_git_after')
    echo "<= End git"
@endtask

{{-- composer --}}
@task('hook_composer_before')
    echo "=> Start composer"
@endtask

@task('hook_composer_after')
    echo "<= End composer"
@endtask

{{-- npm --}}
@task('hook_npm_before')
    echo "=> Start NPM"
@endtask

@task('hook_npm_after')
    @if (!$skip_npm)
        cd {{ $config['npm_path'] }}

        echo "run npm & bower"
        npm install
        bower install --allow-root
        
        echo "run build"
        gulp --production

        echo "remove node_modules & bower_components"
        rm -rf bower_components
        rm -rf node_modules
    @else
        echo "NPM skipped"
    @endif
    echo "<= End NPM"
@endtask

{{-- complete --}}
@task('hook_complete')
    {{-- cd {{ $config['app_path'] }} --}}
    {{-- php {{ $config['composer_path'] }}/wp-cli.phar maintenance-mode deactivate --allow-root --}}
    echo "<== Deployment completed successfully :)"
@endtask