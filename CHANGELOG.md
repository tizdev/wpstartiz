# Changelog

# [2.0.3] 2019-12-17

- Add SSL by default
- Fix: Just active all plugin for second provisionning

# [2.0.2] 2019-12-16

- Move config in config.yml
- Clear default post and set home page
- Set date time format
- Auto install windows ssh key on vagrant box
- Active Url rewriting and permalink
- Add svg-twig-extension install
- Add Custom plugins form tizdev bitbucket repo
- Default config install themetiz

## [2.0.1] 2019-10-04

- Add .env.scotchbox file auto-install
- Add wp cli install and activate lang
- Add active theme + remove default themes/plugin
- Add and active default plugins

## [2.0.0] 2019-10-01

### Added

- Use vagrant provisioning for instal
- Add provision/variables.inc.sh for custom installation
- Add sample dploy
