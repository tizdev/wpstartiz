# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

    config.vm.box = "agencetiz/vmstartiz--lamp"
    # config.vm.box_version = "1.0"
    config.vm.hostname = "scotchbox"
    config.vm.network "forwarded_port", guest: 80, host: 1243
    config.vm.network "private_network", ip: "192.168.33.10"
    config.vm.synced_folder ".", "/var/www", :mount_options => ["dmode=777", "fmode=777"]

    # Performance optimisation
    config.vm.provider "virtualbox" do |vb|
        # Customize the amount of memory on the VM:
        vb.memory = "4096"
        vb.cpus = "2"

        # Enabling multiple cores in Vagrant/VirtualBox
        vb.customize ["modifyvm", :id, "--ioapic", "on"]

        # change the network card hardware for better performance
        vb.customize ["modifyvm", :id, "--nictype1", "virtio" ]
        vb.customize ["modifyvm", :id, "--nictype2", "virtio" ]

        # suggested fix for slow network performance
        # see https://github.com/mitchellh/vagrant/issues/1807
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    end


    # Windows Support
    if Vagrant::Util::Platform.windows?
        config.vm.provision "shell",
        inline: "cd /var/www/provision && find . -type f -print0 | xargs -0 dos2unix",
        run: "always", privileged: false
    end
    if !File.file?("provision/config.yml")
        puts "No config file. Create provision/config.yml and run 'vagrant provision'"
        exit
    end

    # ssh keys
    if File.file?("#{Dir.home}/.ssh/id_rsa")
        config.vm.provision "file", run: "always", source: "#{Dir.home}/.ssh/id_rsa.pub", destination: "~/.ssh/id_rsa.pub"
        config.vm.provision "file", run: "always", source: "#{Dir.home}/.ssh/id_rsa", destination: "~/.ssh/id_rsa"
        config.vm.provision "shell", inline: "chmod 600 ~/.ssh/id_rsa", run: "always", privileged: false
        config.vm.provision "shell", inline: "chmod 644 ~/.ssh/id_rsa.pub", run: "always", privileged: false
        
        # Root ssh key
        config.vm.provision "shell" do |s|
            ssh_prv_key = File.read("#{Dir.home}/.ssh/id_rsa")
            ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
            s.inline = <<-SHELL
                if grep -sq "#{ssh_pub_key}" /root/.ssh/authorized_keys; then
                    echo "SSH keys already provisioned."
                    exit 0;
                fi

                mkdir -p /root/.ssh
                echo #{ssh_pub_key} > /root/.ssh/id_rsa.pub
                echo "#{ssh_prv_key}" > /root/.ssh/id_rsa
            SHELL
        end
        config.vm.provision "shell", inline: "chmod 600 ~/.ssh/id_rsa", run: "always", privileged: true
        config.vm.provision "shell", inline: "chmod 644 ~/.ssh/id_rsa.pub", run: "always", privileged: true
    else
        puts "No SSH key found. You will need to remedy this before pushing to the repository."
    end
    if File.file?("#{Dir.home}/.ssh/config")
        config.vm.provision "file", run: "always", source: "#{Dir.home}/.ssh/config", destination: "~/.ssh/config"
    else
        puts "You have no .ssh/config file."
    end

    config.vm.provision "shell", path: "provision/provision.sh", keep_color: true

end