#!/usr/bin/env bash
#

# Premier lancement de la box lumberjack n'a jamais été lancé
# Détecter si lumberjack est installé
if [[ ! -e ${PATH_PUBLIC}composer.json ]]; then
    alert_info "== Install lumberjack =="
    composer self-update --2
    rm -rf "${PATH_PUBLIC}"
    cd "${PATH_WWW}" || (alert_error "error to go to ${PATH_WWW} lumberjack.sh" && exit)
    # Probleme timeout
    # ${PATH_COMPOSER}lumberjack-bedrock new public --setTimeout=3600 -vvv
    # mv -f ${WP_THEMES_PATH}${WP_THEME_LUMBERJACK} ${WP_THEMES_PATH}${WP_THEME_RENAME}
    # Replacement
    git clone --depth 1 --branch 1.17.1 https://github.com/roots/bedrock.git "${PATH_PUBLIC}"
    alert_success "bedrock cloned"

    cd "${PATH_PUBLIC}"  || (alert_error "error to go to ${PATH_PUBLIC} lumberjack.sh" && exit)
    composer --prefer-dist --with-all-dependencies require rareloop/lumberjack-core
    alert_success "rareloop/lumberjack-core"

    composer --prefer-dist require manuelodelain/svg-twig-extension
 
    # composer fund
    # alert_success "manuelodelain/svg-twig-lumberjack-core"

    #  @no
    ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts
    # echo "${WP_THEMES_BASE_REPO} ${WP_THEMES_PATH}${WP_THEME_RENAME}"
    # cd "${WP_THEMES_BASE_REPO} ${WP_THEMES_PATH}${WP_THEME_RENAME}" ||  (alert_info "error to go to ${WP_THEMES_BASE_REPO} ${WP_THEMES_PATH}${WP_THEME_RENAME} lumberjack.sh  line 29" && exit)
    # git clone git@bitbucket.org:tizdev/wpthemetiz.git
    git clone "${WP_THEMES_BASE_REPO}" "${WP_THEMES_PATH}${WP_THEME_RENAME}"
    # Copie le fichier .env.scotchbox dans /public/.env
    sed -i.bak "s|%WP_URL%|${WP_URL}|g" "${PATH_PROVISION_CONFIG}".env.scotchbox
    cp "${PATH_PROVISION_CONFIG}".env.scotchbox .env
    rm "${PATH_PROVISION_CONFIG}".env.scotchbox
    mv "${PATH_PROVISION_CONFIG}".env.scotchbox.bak "${PATH_PROVISION_CONFIG}".env.scotchbox
    cp "${PATH_PROVISION_CONFIG}"development.php config/environments/development.php
    cp "${PATH_PROVISION_CONFIG}"wp-cli.yml wp-cli.yml
    rm -rf "${PATH_PUBLIC}".github
    rm -rf "${PATH_PUBLIC}".git
    rm -rf "${WP_THEMES_PATH}${WP_THEME_RENAME}"/.github
    rm -rf "${WP_THEMES_PATH}${WP_THEME_RENAME}"/.git
    alert_success "== Lumberjack and theme downloaded successfully =="
fi

# Clone d'un projet déjà installé
# Détecter les composer
if [[ ! -d ${PATH_PUBLIC}vendor/ ]]; then
    # composer bedrock
    cd "${PATH_PUBLIC}" || (alert_error "error to go to ${PATH_PUBLIC} lumberjack.sh" && exit)
    # composer install --prefer-source --with-all-dependencies 
    # composer theme lumberjack
    mkdir -p "${WP_THEMES_PATH}${WP_THEME_RENAME}"
    cd "${WP_THEMES_PATH}${WP_THEME_RENAME}" || (alert_info "error to go to ${WP_THEMES_PATH}${WP_THEME_RENAME} lumberjack.sh" && exit)
    composer --prefer-dist install
    alert_success "lumberjack theme installed"
fi