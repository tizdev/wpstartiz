#!/usr/bin/env bash
#

alert_info "== Init and Install wordpress =="

# Lance l'installation du wordpress avec WP CLI
cd "${WEB_ROOT}" || (alert_error "error to go to  ${WEB_ROOT} wordpress.sh" && exit)
wp core install --url="${WP_URL}" --title="${WP_TITLE}" --admin_user="${WP_ADMIN_USER}" --admin_password="${WP_ADMIN_PASSWORD}" --admin_email="${WP_ADMIN_EMAIL}" --allow-root --skip-email
wp language core install "${WP_LANG}" --allow-root
wp language core activate "${WP_LANG}" --allow-root
wp option update timezone_string "${WP_TIMEZONE}" --allow-root
wp option update date_format "${WP_DATE_FORMAT}" --allow-root
wp option update time_format "${WP_TIME_FORMAT}" --allow-root
wp option update convert_smilies 0 --allow-root

wp comment delete 1 --force --allow-root
wp post delete 1 2 --force --allow-root

wp post create --post_type=page --post_title="${WP_FRONT_PAGE}" --post_content='Contenu de la page automatique' --post_status=publish --allow-root
wp option update page_on_front "$(wp post list --post_type=page --post_status=publish --posts_per_page=1 --pagename="${WP_FRONT_PAGE}" --field=ID --format=ids --allow-root)" --allow-root
wp option update show_on_front 'page' --allow-root

# Activation du theme 
wp theme activate "${WP_THEME_RENAME}" --allow-root

# suppression des thèmes/plugins inutile
cd "${WEB_ROOT}"/wp/wp-content/themes/ || (alert_error "error to go to  ${WEB_ROOT}/wp/wp-content/themes/ wordpress.sh" && exit)
rm -rf ./*glob*

# Install plugin à la première installation
if [ -f "${PATH_PUBLIC}composer.json" ]; then
    cd "${WEB_ROOT}"/wp/wp-content/plugins/ || (alert_error "error to go to  ${WEB_ROOT}/wp/wp-content/plugins/ wordpress.sh" && exit)
    rm hello.php

    # install et active les plugins par default
    if [ "${#WP_INSTALL_PLUGINS[@]}" -ne 0 ]; then
        cd "${PATH_PUBLIC}" || (alert_error "error to go to  ${PATH_PUBLIC} wordpress.sh" && exit)
        FOR_COMPOSER_INSTALL=""
        FOR_WP_INSTALL=""
        for PLUGIN in "${WP_INSTALL_PLUGINS[@]}"; do 
            FOR_COMPOSER_INSTALL="wpackagist-plugin/${PLUGIN}"
            FOR_WP_INSTALL="${PLUGIN}"
            composer --prefer-source require "${FOR_COMPOSER_INSTALL}"
            wp plugin activate "${FOR_WP_INSTALL}" --allow-root
        done
    fi

    # install et active les plugins premium tiz
    if [ "${#WP_INSTALL_CUSTOM_PLUGIN[@]}" -ne 0 ]; then
        # ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts
        cd "${WP_PLUGINS_PATH}" || (alert_error "error to go to  ${WP_PLUGINS_PATH} wordpress.sh" && exit)
        FOR_WP_INSTALL=""
        
        for PLUGIN in "${WP_INSTALL_CUSTOM_PLUGIN[@]}"; do 
            if [ -d "${PLUGIN}" ]; then
                alert_warning "Plugin ${PLUGIN} déjà installé"
            else
                git clone git@bitbucket.org:tizdev/"${PLUGIN}".git
                GIT_STATUT=$?

                if [ "${GIT_STATUT}" == "0" ]; then
                    FOR_WP_INSTALL+=" ${PLUGIN}"
                    rm -rf "${PLUGIN}"/.git
                    echo "!web/app/plugins/${PLUGIN}">> "${PATH_PUBLIC}/.gitignore"
                fi
            fi
        done

        if [ -n "${FOR_WP_INSTALL}" ]; then
            cd "${WEB_ROOT}" || (alert_error "error to go to  ${WEB_ROOT} wordpress.sh" && exit)
            wp plugin activate ${FOR_WP_INSTALL} --allow-root
        fi
    fi
else
    wp plugin activate --all --allow-root
fi

cd "${WEB_ROOT}" || exit
alert_info "Generate permalink"
wp rewrite structure "${WP_PERMALINK}" --skip-plugins=acf-component-field --hard --allow-root

alert_success "== Init and Install wordpress successful =="