#!/usr/bin/env bash
#
# update WP-CLI
# since Scotch Box pro (php7), we have to reinstall WP-CLI
source /var/www/provision/utils.sh
source /var/www/provision/variables.inc.sh
source "${PATH_PROVISION}"vendors/alert.sh
# composer self-update --2

# alert_info "== Update WP CLI (re-install) =="
# sudo rm /usr/local/bin/wp
# sudo curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
# sudo chmod +x wp-cli.phar
# sudo mv wp-cli.phar /usr/local/bin/wp
# alert_success "WP-CLI Update successful"
# composer global require hirak/prestissimo

# Install root dependencies
cd "${PATH_WWW}" || (alert_error "error to go to ${PATH_WWW} provision.sh" && exit)
composer install
alert_success "Root dependencies installed"


source "${PATH_PROVISION}"lumberjack.sh
source "${PATH_PROVISION}"wordpress.sh