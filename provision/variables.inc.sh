#!/bin/bash
#
# variables.inc.sh
#

# Path
# ---------------------------------------
PATH_A2_SITES_AVAILABLE="/etc/apache2/sites-available/"
PATH_COMPOSER="/home/vagrant/.config/composer/vendor/bin/"
PATH_WWW="/var/www/"
PATH_PUBLIC="/var/www/public/"
PATH_PROVISION="/var/www/provision/"
PATH_PROVISION_APACHE="${PATH_PROVISION}apache/"
PATH_PROVISION_CONFIG="${PATH_PROVISION}config/"
PATH_VAGRANT="/home/vagrant/"

WEB_ROOT="/var/www/public/web"
WP_THEMES_PATH="${WEB_ROOT}/app/themes/"
WP_PLUGINS_PATH="${WEB_ROOT}/app/plugins/"

eval "$(parse_yaml ${PATH_PROVISION}config.yml "WP_")"
