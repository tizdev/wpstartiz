# WP Startiz  
(https://bitbucket.org/tizdev/wpstartiz/src/master/)

---

- Wordpress [latest]

---

## Table of contents

* [About the project](#markdown-header-about-the-project)
    * [Description](#markdown-header-description)
    * [Feature](#markdown-header-feature)
    * [Plugins](#markdown-header-plugins)
    * [Environments](#markdown-header-environments)
* [Getting started](#markdown-header-getting-started)
    * [Prerequisites](#markdown-header-prerequisites)
    * [Installation](#markdown-header-installation)
    * [Credentials](#markdown-header-credentials)
* [Production](#markdown-header-production)
    * [Create the environments](#markdown-header-create-the-environments) 
* [Test and deploy](#markdown-header-test-and-deploy)
    * [Deploy method](#markdown-header-deploy-method)   
* [Ressources](#markdown-header-ressources)
    * [Pro Features](#markdown-header-pro-features)

---

## About the project

### Description

WP Startiz is an automatic luncher box for wordpress based on **Vagrant**, **VMStartiz**, **Roots/bedrock**.


### Feature
- provision configuration with variables
- Installation [bedrock](https://www.youtube.com/watch?v=2lcRaKiw1-I) stack
- Installation of custom theme (based on git clone)
- Installation [lumberjack](https://lumberjack.rareloop.com/) create theme with mvc method
- Installation [timber](https://www.youtube.com/watch?v=o-io2Ck9iDk&t=693s) create theme with Twig 

### Plugins
  * contact-form-7
  * wordpress-seo
  * flamingo
  * google-analytics-for-wordpress
  * quantcast-choice

Plugin need licence key

  - advanced-custom-field-pro
  - acf-component-field
 
 
### Environments

 * local : http://local.test
 * Preprod :  https://example.tiz.fr
 * Prod : https://example.com/

Back-Office : https://local.test/wp/wp-admin

---

## Getting started
### Prerequisites

- Need [Vagrant](https://www.youtube.com/watch?v=mRgiFZZG4pk) in your computer
- Update host file in your computer 
- Go to C:\Windows\System32\drivers\etc
- Add in host file : 192.168.33.10	local.test

---

This is an example of how to list things you need to use the software and how to install them.

* npm
```sh
$ npm install npm@latest -g
```

### Installation

1. get the project "wpstartiz" in bitbucket
2. run `cd provision`
3.  copy and complete config.sample.yml -> config.yml
4. If you don't have licence key for custom plugin : comment in config.yml the part of **INSTALL_CUSTOM_PLUGIN**
5. run `vagrant up`
6. copy and complete .env.example -> .env
7. Add licence key in .env (if you don't have comment it in config.yml) 
8. remove `public/*` in root `.gitignore`

...

### Credentials
Available in Lastpass, and in the Google Drive

---

## Production

### Create the environments

1. Create the VHOST and the folders structure on servers with the following model :
    - `prod/<domain>/<subdomain>/public/web` (exemple for `www.tiz.fr` : `prod/tiz.fr/www/public/web`)
    - `preprod/<domain>/<subdomain>/public/web` (exemple for `stsa.tiz.fr` : `preprod/tiz.fr/stsa/public/web`)
2. Add a `A` entry to [Gandhi](https://id.gandi.net/fr/) for exemple `<exemple>.tiz.fr`
3. On the server create an SSH local key (if it does not already exists) (```ssh-keygen -t rsa```)
4. Ask Tiz to add the previous SSH key (`id_rsa.pub`) to bitbucket (send an email with the content of id_rsa.pub and followinf infos)
    - Server IP / 
    - Server type : Mutu / VPS / Dédié
    - name of server : exple vps-e75655b1
    - url of prod website : exple www.efluid.com
    - username that generared the ssh key : exple root
5. Clone the git repository into the folders (stage + prod)
    - on **prod** checkout on branch **master**
    - on **preprod** checkout on branch **staging/stage**


---

## Test and deploy

### Deploy method

`envoiz`

1. complete .envoiz.config.yml
2. In local : `vendor/bin/envoiz deploy prod --no_npm` 
3. If command don't work use : `php ./vendor/bin/envoiz deploy prod --no_npm`
4. Verify your site  

---

## Ressources

Box  [vmstartiz--lamp](https://bitbucket.org/tizdev/vmstartiz-lamp/src) is based on :

- [Scotchbox](https://github.com/scotch-io/scotch-box)
- [Scotchbox build scripts](https://github.com/scotch-io/scotch-box-build-scripts)